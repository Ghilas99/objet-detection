# Objet Détection


# Dataset
- train-> 62 images 
- test -> 24 images 

# how to run the program 
- step 1: install anconda or miniconda
- step 2: create env
    ```bash
    conda create -n myenv python=3.9
    conda activate myenv
    ```
- step 3: install tools
    ```bash
    pip install numpy
    pip install torch
    pip install matplotlib
    pip install detecto
    ```
- step 4: run it
    ```bash
    python3 Detecto.py
    python3 Detecto-predicat.py #to test another images 
    ```

# Or us note book file  Detecto.ipynb (c'est un resumé des deux files)



