# Simple object detection using Detecto library from AlanBi
#
# https://www.analyticsvidhya.com/blog/2021/06/simplest-way-to-do-object-detection-on-custom-datasets/

import os
os.environ["KMP_DUPLICATE_LIB_OK"] = "TRUE"

import numpy as np
import torch

from detecto import core, utils, visualize
from detecto.visualize import show_labeled_image, plot_prediction_grid
from torchvision import transforms


model = core.Model.load('model_weights.pth', ['velo', 'trottinette'])

# test on an  trottinette image

image = utils.read_image('testModel/velo-trotinette.jpg') 
predictions = model.predict(image)
labels, boxes, scores = predictions
#show_labeled_image(image, boxes, labels)

# threshold for a valid detection
thresh=0.30
filtered_indices=np.where(scores>thresh)
filtered_scores=scores[filtered_indices]
filtered_boxes=boxes[filtered_indices]
num_list = filtered_indices[0].tolist()
filtered_labels = [labels[i] for i in num_list]
show_labeled_image(image, filtered_boxes, filtered_labels)

